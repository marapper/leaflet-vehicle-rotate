//TODO сделать классом для применения в виде маркера
var triangle = function(){
    this._createOffsetTriangle = function(length, width) {
        //inline circle radius
        //work unproperly
        //not real center of triangle?
        // var a = length;
        // var b = width/2;
        // var c = Math.sqrt(a*a + b*b);
        // var S = length*width;
        // var p = (width + c + c) / 2;
        // var r = S / p;

        return [
                    [length / 3 * 2, 0],
                    [- length / 3, width/2],
                    [- length / 3, -width/2]
                ];
    };

    //TODO лучшекеширующе? если понадобятся маркеры разных размеров?
    var length = 0.002;
    var width = 0.0013;

    return this._createOffsetTriangle(length, width);
}();

L.Polygon.RotatedPolygon= L.Polygon.extend({ 
    _reset: function() {
        var x0 = this._center.lat;
        var y0 = this._center.lng;

        var points = triangle;
        for(var i in points){
            points[i][0] += x0;
            points[i][1] += y0;
        }

        this.setLatLngs(points);

        /*не работает, т.к на самом деле поврехность Path->Polygon больше чем сам полигон */
        var cssTransform = 'rotate(' + this.options.direction + 'deg)';
        // rotate
        var cssTransform = 'rotate(' + this.options.direction + 'deg)';

        if(typeof(this._path.style.WebkitTransform) != 'undefined'){
            this._path.style.WebkitTransform = this._path.style.WebkitTransform.replace(/\s*rotate\([^)]+\)/, '');
        }
        this._path.style.WebkitTransform = this._path.style.WebkitTransform + ' ' + cssTransform;
        if(typeof(this._path.style.MozTransform) != 'undefined'){
            this._path.style.MozTransform = this._path.style.MozTransform.replace(/\s*rotate\([^)]+\)/, '');
        }
        this._path.style.MozTransform = this._path.style.MozTransform  + ' ' + cssTransform;
        //TODO pos как в CSS?
        //this._path.style.MozTransform = L.DomUtil.getTranslateString(pos) + ' ' + cssTransform;
        this._path.style.MsTransform = cssTransform; //FIXME the same thing here?
        this._path.style.OTransform = cssTransform; //FIXME the same thing here?

        //this._path.style.zIndex = pos.y;
    },
    getDirection: function () {
        return this.options.direction;
    },
    setDirection: function (direction) {
        //TODO direction must be in 0..360
        this.options.direction = direction;

        if (this._map) {
            this._reset();
        }
    },
    getPosition: function () {
        return this._center;
    },
    move: function(point, direction) {
        this._center = new L.LatLng(point[0], point[1]);
        
        this.setDirection(direction);
    },
    onAdd: function (map){
        this._map = map;
        L.Polygon.prototype.onAdd.call(this, map);

        this._center = this._latlngs;
        
        this.setDirection(this.options.direction);
        //TODO надо так же подхватывать это событие?
        //map.on('viewreset', this._reset, this);
    }

});

//proxy marker to polygon
//FIXME memory?
L.Marker.RotatedMarker= L.Marker.extend({    
    _reset: function() {
      
    },
    move: function(point, direction) {
        this.options.polygon.move(point, direction);
    },
    getPosition: function () {
        return this.options.polygon.getPosition();
    },
    getDirection: function () {
        return this.options.polygon.getDirection();
    },
    setDirection: function (direction) {
        //TODO direction = undefined ?
        this.options.polygon.setDirection(direction);

        if (this._map) {
            this._reset();
        }
    },
    onRemove: function (map){
        //TODO remove poly
    },
    onAdd: function (map){
        this._map = map;

        //TODO check options.* exists
        this.options.polygon = new L.Polygon.RotatedPolygon(this._latlng, {
                color: this.options.color, opacity: 0.6,
                fillColor: this.options.color, fillOpacity: 0.9,
                //TODO size для размера стрелочки масштабирование
                //TODO weight для того чтобы при масштабировании все равно видно было
                //TODO и предусмотреть динамическое масштабирование
                weight: this.options.weight,
                direction: this.options.direction
            });

        this.options.polygon.addTo(this._map);

        this._reset();
    }

});