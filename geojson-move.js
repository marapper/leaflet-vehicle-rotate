function geoJsonMove(map, cars, createMarkerClosure){
	var markerCars = {};//на VR-AS мы должны отличать ТС по ключу-идентификатору ТС

	//эмулируем наш REST
    var geo = cars.getGeoJson();

    var bentchmark = new Bentchmark();
    //добавление слоя
    var layer = L.geoJson( geo , {
        pointToLayer: function (feature, latlng) {
            var marker = createMarkerClosure(feature, latlng)
            
            //TODO может быть, есть способ напрямую из лейера получать объекты?
            //нет, судя по http://stackoverflow.com/questions/9912145/leaflet-how-to-find-existing-markers-and-delete-markers
            markerCars[feature.properties.id] = marker;

            if(feature.properties.id == cars.getCount()){
                bentchmark.log('GeoJson created');
            }

            return marker;
        }
    });

    map.addLayer(layer, true);
    //TODO еще один способ - myLayer.addData(geojsonFeature);, но надо все равно перед этим удалять все
    //https://github.com/Leaflet/Leaflet/issues/1416
    //geometryToLayer( <GeoJSON> featureData, <Function> pointToLayer? )
    //http://leafletjs.com/reference.html#geojson
    //в нашем же случае можно передвигать только часть по желанию

    //TODO bringToFront понадобится на карте аналитики
    
    setInterval(function(){
        cars.move();

        //эмулируем наш REST
        var geo = cars.getGeoJson();
        for(var i in geo){
            var coord = [
                geo[i].geometry.coordinates[1], //IMPORTANT в GeoJson в отличии от лифлета
                geo[i].geometry.coordinates[0], // широта/долгота поменяна местами
            ];

            //TODO есть ли возможность замерить как-то скорость перерисовки маркеров?

            markerCars[ geo[i].properties.id ].move(coord, geo[i].properties.direction);

            //можно использовать для того, чтобы увидеть движение маркера
            // var marker = new L.Marker.RotatedMarker(coord, {
            //     color: geo[i].properties.color, 
            //     weight: 2,
            //     direction: geo[i].properties.direction,
            // });
            // marker.addTo(map);
        }

        // без GeoJson
        // var veh = cars.getAll();
        // for(var k in veh){
        //     var coord = [
        //         veh[k].curCoordinate[1],
        //         veh[k].curCoordinate[0],
        //     ];

        //     //TODO есть ли возможность замерить как-то скорость перерисовки маркеров?

        //     // markerCars[ geo[i].properties.id ].move(coord, geo[i].properties.direction);

        //     //можно использовать для того, чтобы увидеть движение маркера
        //     var marker = new L.Marker.RotatedMarker(coord, {
        //         color: veh[k].color, 
        //         weight: 2,
        //         direction: veh[k].direction,
        //     });
        //     marker.addTo(map);
        // }
        
    }, 100);
}