L.Marker.RotatedMarker= L.Marker.extend({    
    _reset: function() {
        var pos = this._map.latLngToLayerPoint(this._latlng).round();

        L.DomUtil.setPosition(this._icon, pos);
        if (this._shadow) {
            L.DomUtil.setPosition(this._shadow, pos);
        }

        var cssTransform = 'rotate(' + this.options.direction + 'deg)';

        this._icon.style.WebkitTransform = this._icon.style.WebkitTransform + ' ' + cssTransform;
        //this._icon.style.MozTransform = this._icon.style.MozTransform  + ' ' + cssTransform;
        this._icon.style.MozTransform = L.DomUtil.getTranslateString(pos) + ' ' + cssTransform;
        this._icon.style.MsTransform = cssTransform; //FIXME the same thing here?
        this._icon.style.OTransform = cssTransform; //FIXME the same thing here?
        
        this._icon.style.zIndex = pos.y;
    },
    getDirection: function () {
        return this.options.direction;
    },
    setDirection: function (direction) {
        //TODO direction must be in 0..360
        if (this._map) {
            this._removeIcon();
        }

        this.options.direction = direction;

        if (this._map) {
            this._initIcon();
            this._reset();
        }
    },
    getPosition: function () {
        return this._latlng;
    },
    move: function(point, direction) {
        this._latlng = point;
        
        //TODO direction = undefined ?
        this.setDirection(direction);
    },
    onAdd: function (map){
        this._map = map;
        L.Marker.prototype.onAdd.call(this, map);
        
        this.setDirection(this.options.direction);
    }

});