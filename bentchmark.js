function Bentchmark(){
   this.beginTime = new Date();
}

Bentchmark.prototype = {
   time : function(){
      return (new Date()).getTime() - this.beginTime.getTime();
   },
   log: function(message){
		console.log(message + ': ' + this.time() + 'мс');
   }
}