//TODO сделать классом для применения в виде маркера
var triangles = function(){
    this._createOffsetTriangle = function(length, width) {
        //inline circle radius
        //work unproperly
        //not real center of triangle?
        // var a = length;
        // var b = width/2;
        // var c = Math.sqrt(a*a + b*b);
        // var S = length*width;
        // var p = (width + c + c) / 2;
        // var r = S / p;

        this._triangle = [
                    [length / 3 * 2, 0],
                    [- length / 3, width/2],
                    [- length / 3, -width/2]
                ];
    };

    this._rotateTriangle = function(direction) {
        var points = [];
        for(var i in this._triangle){
            var offsetPoint = this._rotateOffset(this._triangle[i], direction);

            points.push(offsetPoint);
        }

        return points;
    };

    this._toRadians = function (angle) {
      return angle * Math.PI / 180;
    };

    this._rotateOffset = function(offset, direction) {
        var dx = offset[0];
        var dy = offset[1];
        var _dx = dx, _dy = dy;

        if(direction) {
            var angle = this._toRadians(direction);

            _dx = dx*Math.cos(angle) - dy*Math.sin(angle);
            _dy = dx*Math.sin(angle) + dy*Math.cos(angle);
        }

        var koef = 1.5;//1 lat <> 1 long

        return [
            _dx / 10000,//TODO для предотвращения ошибки в вычислениях, вынести
            _dy / 10000 * koef,
        ];
    };

    //TODO лучшекеширующе? если понадобятся маркеры разных размеров?
    var length = 0.002 * 10000;//TODO для предотвращения ошибки в вычислениях, вынести
    var width = 0.0013 * 10000;

    this._createOffsetTriangle(length, width);

    var arr = {};
    for(var dir=0; dir<=360; dir++) {
        var rotated = this._rotateTriangle(dir);
        arr[dir] = rotated;
    }

    return arr;
}();

L.Polygon.RotatedPolygon= L.Polygon.extend({ 
    _reset: function() {
        var x0 = this._center.lat;
        var y0 = this._center.lng;

        var points = triangles[this.options.direction];
        var newPoints = [];

        for(var key in points){
            newPoints.push([
                points[key][0] + x0,
                points[key][1] + y0 
            ]);
        }

        this.setLatLngs(newPoints);
    },
    getDirection: function () {
        return this.options.direction;
    },
    setDirection: function (direction) {
        //TODO direction must be in 0..360
        this.options.direction = direction;

        if (this._map) {
            this._reset();
        }
    },
    getPosition: function () {
        return this._center;
    },
    move: function(point, direction) {
        if(
            this.getDirection() == direction &&
            this._center.lat == point[0] &&
            this._center.lng == point[1]
        ){
            return;//do nothing
        }

        //TODO посмотреть по памяти, обязательно ли создавать объект
        this._center = new L.LatLng(point[0], point[1]);
        
        this.setDirection(direction);
    },
    onAdd: function (map){
        this._map = map;
        L.Polygon.prototype.onAdd.call(this, map);

        this._center = this._latlngs;

        this.setDirection(this.options.direction);
        //TODO надо так же подхватывать это событие?
        //map.on('viewreset', this._reset, this);
    }

});

//proxy marker to polygon
//FIXME memory?
L.Marker.RotatedMarker= L.Marker.extend({    
    _reset: function() {
        
    },
    move: function(point, direction) {
        this.options.polygon.move(point, direction);
    },
    getPosition: function () {
        return this.options.polygon.getPosition();
    },
    getDirection: function () {
        return this.options.polygon.getDirection();
    },
    setDirection: function (direction) {
        this.options.polygon.setDirection(direction);

        if (this._map) {
            this._reset();
        }
    },
    onRemove: function (map){
        //TODO remove poly
        this._map.removeLayer(this.options.polygon);
    },
    onAdd: function (map){
        this._map = map;

        //TODO check options.* exists
        this.options.polygon = new L.Polygon.RotatedPolygon(this._latlng, {
                color: this.options.color, opacity: 0.6,
                fillColor: this.options.color, fillOpacity: 0.9,
                //TODO size для размера стрелочки масштабирование
                //TODO weight для того чтобы при масштабировании все равно видно было
                //TODO и предусмотреть динамическое масштабирование
                weight: this.options.weight,
                direction: this.options.direction
            });

        this.options.polygon.addTo(this._map);

        this._reset();
    }

});