//фейк для эмуляции REST
var Vehicles = function(center, dLat, dLng){
	center = new L.LatLng(center[0], center[1]);
	var vehicles = [];
	var num = 0;

	var _toRadians = function (angle) {
      return angle * Math.PI / 180;
    };

	//предрассчитыаем синкосинусы с минимальным шагом 0.1
	var MOVE_ANGLE_STEP = 0.1;
	var MOVE_ANGLE_PRECISION = 1;

	var _sin = {};
	for(var dir=0; dir<=360; dir++){
		for(var i=0; i<= 1 / MOVE_ANGLE_STEP; i++){
			var d = dir + i * MOVE_ANGLE_STEP;
			d = d.toFixed(MOVE_ANGLE_PRECISION);
			var angle = _toRadians( d );
			_sin[ d ] = Math.sin(angle);
		}
	}

	var _cos = {};
	for(var dir=0; dir<=360; dir++){
		for(var i=0; i<= 1 / MOVE_ANGLE_STEP; i++){
			var d = dir + i * MOVE_ANGLE_STEP;
			d = d.toFixed(MOVE_ANGLE_PRECISION);
			var angle = _toRadians( d );
			_cos[ d ] = Math.cos(angle);
		}
	}

	var getRandom = function(min, max) {
	  return Math.random() * (max - min) + min;
	}

	var getRandomInt = function (min, max) {
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}


	var getRandomColor= function() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.round(Math.random() * 15)];
	    }
	    return color;
	}

	var _rotate = function(point, angle) {
		var PRECISION_ERROR = 10000;

        var dx = point[1] * PRECISION_ERROR; //IMPORTANT в GeoJson в отличии от лифлета
        var dy = point[0] * PRECISION_ERROR; // широта/долгота поменяна местами
        var _dx = dx, _dy = dy;

        angle = angle.toFixed(MOVE_ANGLE_PRECISION);

        _dx = dx*_cos[angle] - dy*_sin[angle];
        _dy = dx*_sin[angle] + dy*_cos[angle];
        //FIXME как-то с предрасчитанными все резко, может, оптимизации V8 хватит на расчет синусов?
        // angle = _toRadians(angle);
        // _dx = dx*Math.cos(angle) - dy*Math.sin(angle);
        // _dy = dx*Math.sin(angle) + dy*Math.cos(angle);

        return [
            _dy / PRECISION_ERROR,
            _dx / PRECISION_ERROR,
        ];
    };

	return {
		create: function(numStay, numMove){
			num = numMove;
			for(var i=0; i < numStay + numMove; i++){
				var vehicle = {
					id: (i + 1), //тот самый id
					color: getRandomColor(),
					dCoordinate: [
						getRandom(-dLng, dLng), //IMPORTANT в GeoJson в отличии от лифлета
						getRandom(-dLat, dLat)  // широта/долгота поменяна местами
					],
					direction: getRandomInt(0, 360),

					dirSpeed: getRandomInt(1, 10),
					
					//movement
					moveAngle: getRandomInt(0, 360),
					moveDir: (getRandomInt(0, 1) == 1) ? 1 : -1, //по часовой или против
					speed: getRandomInt(5, 40) * MOVE_ANGLE_STEP,
				};

				vehicle.curCoordinate = [
					center.lng + vehicle.dCoordinate[0],
					center.lat + vehicle.dCoordinate[1]
				];

				vehicles.push(vehicle);
			}
		},
		getGeoJson: function(){
			var geoJson = [];

			for(var i in vehicles){
				var geo = {
				    type: "Feature",
				    properties: {
				    	id: vehicles[i].id,
				        color: vehicles[i].color,
				        direction: vehicles[i].direction
				    },
				    geometry: {
				        type: "Point",
				        coordinates: vehicles[i].curCoordinate
				    }
				};

				geoJson.push(geo);
			}

			return geoJson;
		},
		getAll: function(){
			return vehicles;
		},
		getCount: function(){
			return vehicles.length;
		},
		move: function(){
			for(var j=0; j<num; j++){
				//rotate
				vehicles[j].direction += vehicles[j].moveDir * vehicles[j].dirSpeed;
				vehicles[j].direction = (vehicles[j].direction < 0) ? 360 : (vehicles[j].direction % 361);

				//circle move
				vehicles[j].moveAngle += vehicles[j].moveDir * vehicles[j].speed; 
				vehicles[j].moveAngle = (vehicles[j].moveAngle < 0) ? 360 + vehicles[j].moveAngle : (vehicles[j].moveAngle % 361);

				var cur = _rotate(vehicles[j].dCoordinate, vehicles[j].moveAngle);
				cur = [
					(center.lng + cur[0]), //IMPORTANT в GeoJson в отличии от лифлета
				    (center.lat + cur[1])  // широта/долгота поменяна местами
				];
				vehicles[j].curCoordinate = cur;
			}
		}
	};
};