function createMap(coord, zoom){
    var map = L.map('map').setView(coord, zoom);

    L.tileLayer('http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png', {
        minZoom: 4,
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="http://cloudmade.com">CloudMade</a>'
    }).addTo(map);


    //var marker = L.marker([55.685, 37.55]).addTo(map);

    var popup = L.popup();


    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("Coordinates " + e.latlng.toString())
            .openOn(map);
    }
    map.on('click', onMapClick);

    return map;
};